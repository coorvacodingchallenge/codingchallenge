import { createMuiTheme } from '@material-ui/core/styles';
import * as colors from '@material-ui/core/colors';

const baseTheme = {
  palette: {
    background: {
      default: colors.blueGrey['50'],
    },
    header: {
      primary: colors.lightBlue['700'],
      secundary: colors.lightBlue['600'],
    }
  },
};

//Theme must be wrapped in funciton getMuiTheme
export default createMuiTheme(baseTheme);
