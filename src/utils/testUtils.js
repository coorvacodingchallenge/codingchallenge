import React from 'react';
import { Provider } from 'react-redux';
import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import theme from '../material_ui_raw_theme_file.js';

// Creacion del store
import configureStore from '../store/config';

const store = configureStore({});

export const mockState = (WrappedComponent) => {
  return class extends React.Component {
    render() {
      return (
        <Provider store={store}>
          <MuiThemeProvider theme={theme}>
            <WrappedComponent {...this.props} />
          </MuiThemeProvider>
        </Provider>
      );
    }
  };
}