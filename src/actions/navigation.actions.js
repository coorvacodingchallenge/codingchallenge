import { push } from 'react-router-redux'

export function routing(entityId) {
  return (dispatch, getState) => {
    dispatch(push(`/contacts/detail/?${entityId}`));
  }  
}
