import ServerConfig from '../config/server';
import httpStatus from 'http-status';

const statusCodes = {
  GET:      [httpStatus.OK],
  POST:     [httpStatus.OK, httpStatus.CREATED],
  PUT:      [httpStatus.OK],
  DELETE:   [httpStatus.OK, httpStatus.NO_CONTENT],
  OPTIONS:  [httpStatus.OK, httpStatus.NO_CONTENT]
}

class Api {
  static headers() {
    return {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
      'dataType': 'json',
    }
  }

  static async get(route) {
    return this.xhr(route, null, 'GET');
  }

  static async put(route, params) {
    return this.xhr(route, params, 'PUT')
  }

  static async post(route, params) {
    return this.xhr(route, params, 'POST')
  }

  static async delete(route, params) {
    return this.xhr(route, params, 'DELETE')
  }

  static async xhr(route, params, verb) {
    const host = ServerConfig.backend_DIR;
    const url = `${host}${route}`;
    let options = Object.assign({ method: verb }, params ? { body: JSON.stringify(params) } : null );
    // options.headers = Api.headers();

    return fetch(url, options).then( resp => {
      let json = resp.json();
      const expectedCode = statusCodes[verb];
      if (expectedCode.includes(resp.status)) {
        return json;
      }
      return json.then(err => {throw err});
    }).then( json => json );
  }
}
export default Api
