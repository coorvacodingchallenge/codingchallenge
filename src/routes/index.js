import MainLayoutApp from '../components/MainLayoutApp';

var indexRoutes = [
  { path: "/", name: "Home", component: MainLayoutApp }
];

export default indexRoutes;
