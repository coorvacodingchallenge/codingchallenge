import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import { createBrowserHistory } from "history";
import indexRoutes from "./index";

// Creacion del history
const history = createBrowserHistory();

class MasterRouter extends React.Component {
  render() {
    return (
      <Router history={history}>
        <Switch>
          {indexRoutes.map((prop, key) => {
            return <Route exact path={prop.path} component={prop.component} key={key} />;
          })}
        </Switch>
      </Router>
    );
  }
}

export default MasterRouter;