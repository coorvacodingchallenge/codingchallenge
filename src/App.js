import React, { Component } from 'react';

import { Provider } from 'react-redux';
import configureStore from './store/config';

import MasterRouter from './routes/MasterRouter';

import MuiThemeProvider from '@material-ui/core/styles/MuiThemeProvider';
import theme from './material_ui_raw_theme_file.js';

// Creacion del store
const store = configureStore({});

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MuiThemeProvider theme={theme}>
          <MasterRouter/>
        </MuiThemeProvider>
      </Provider>
    );
  }
}

export default App;
