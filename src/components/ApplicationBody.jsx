import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

import { Route, Switch } from "react-router-dom";

import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { ActionCreators } from '../actions'; 

import ContactList from "./ContactList";
import ContactDetail from "./ContactDetail";

const styles = theme => ({
  root: theme.mixins.gutters({
    paddingTop: 16,
    paddingBottom: 16,
    marginTop: theme.spacing.unit * 3,
  }),
});


class ApplicationBody extends React.Component {
  constructor() {
    super()
    this.state = {
    }
  }

  render() {
    const { classes } = this.props;

    return (
      <Paper className={classes.root} elevation={4}>
          <Switch>
            <Route exact path="/" render={ props => <ContactList {...this.props}/> } />
            <Route exact path="/contacts/detail/:entityId"  render={ props => <ContactDetail {...this.props}/> } />
          </Switch>
      </Paper>
    )
  }
}

ApplicationBody.propTypes = {
  classes: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    navigator: state.navigator,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(ActionCreators, dispatch),
    dispatch: dispatch
  };
}

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps))(ApplicationBody);
