import * as React from 'react';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';
import IconButton from '@material-ui/core/IconButton';
import FilterListIcon from '@material-ui/icons/FilterList';
import RefreshIcon from '@material-ui/icons/Refresh';
import EditIcon from '@material-ui/icons/Edit';

import {
  PagingState,
  SortingState,
  CustomPaging,
  IntegratedSorting,
  IntegratedPaging,
  FilteringState,
  EditingState,
} from '@devexpress/dx-react-grid';
import {
  Grid,
  Table,
  TableHeaderRow,
  PagingPanel,
  Toolbar,
  TableColumnVisibility,
  TableFilterRow,
  TableEditColumn,
} from '@devexpress/dx-react-grid-material-ui';

import {
  Template, TemplatePlaceholder, Plugin, TemplateConnector,
} from '@devexpress/dx-react-core';

import { Loading } from './loading';

import Api from '../lib/Api';

const FilterToggle = props => {
  const { toggleFilter } = props;

  return (
    <Plugin name="FilterToggle">
      <Template name="toolbarContent">
        <TemplatePlaceholder />
        <TemplateConnector>
          {() => (
            <React.Fragment>
              <Tooltip title='Mostrar/Ocultar Filtros' placement='bottom' enterDelay={300}>
                <IconButton onClick={toggleFilter}>
                  <FilterListIcon />
                </IconButton>
              </Tooltip>
            </React.Fragment>
          )}
        </TemplateConnector>
      </Template>
    </Plugin>
  );
}

const RefreshToggle = props => {
  const { updateTable } = props;

  return (
    <Plugin name="RefreshToggle">
      <Template name="toolbarContent">
        <TemplatePlaceholder />
        <TemplateConnector>
          {() => (
            <React.Fragment>
              <Tooltip title='Actualizar la tabla' placement='bottom' enterDelay={300}>
                <IconButton onClick={updateTable}>
                  <RefreshIcon />
                </IconButton>
              </Tooltip>
            </React.Fragment>
          )}
        </TemplateConnector>
      </Template>
    </Plugin>
  );
}

const EditButton = ({ onExecute }) => (
  <IconButton onClick={onExecute} title="Edit row">
    <EditIcon />
  </IconButton>
);

const commandComponents = {
  edit: EditButton,
};

const Command = ({ id, onExecute }) => {
  const CommandButton = commandComponents[id];
  return (
    <CommandButton
      onExecute={onExecute}
    />
  );
};

class ContactList extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      columns: [
        { name: 'first_name', title: 'First Name' },
        { name: 'last_name', title: 'Last Name' },
        { name: 'email', title: 'Email' },
      ],
      rows: [],
      sorting: [{ columnName: 'first_name', direction: 'asc' }],
      totalCount: 0,
      pageSize: 10,
      pageSizes: [10, 20, 0],
      currentPage: 0,
      loading: true,
      showFilters: false,
      filters: [],
    };

    this.changeSorting = this.changeSorting.bind(this);
    this.changeCurrentPage = this.changeCurrentPage.bind(this);
    this.changePageSize = this.changePageSize.bind(this);
    this.showFiltersHandler = () => this.setState({ showFilters: !this.state.showFilters });
    this.changeFilters = this.changeFilters.bind(this);
    this.toggleFilter = this.toggleFilter.bind(this);
    this.changeEditingRowIds = editingRowIds => this.detailHandler(editingRowIds);
    this.commitChanges = () => console.log('commit Changes');;
  }

  componentDidMount() {
    this.loadData();
  }

  changeSorting(sorting) {
    this.setState({
      sorting
    });
  }
  changeCurrentPage(currentPage) {
    this.setState({
      currentPage,
    });
  }

  changeFilters(filters) {
    this.setState({
      filters,
    });
  }

  changePageSize(pageSize) {
    const totalPages = Math.ceil(this.state.totalCount / pageSize);
    const currentPage = Math.min(this.state.currentPage, totalPages - 1);

    this.setState({
      pageSize,
      currentPage,
    });
  }

  toggleFilter() {
    this.setState({ showFilters: !this.state.showFilters })
  }
  
  queryString(search) {
    const { filters } = this.state;

    let queryString = '/contacts/';

    if (search && filters && filters.length > 0) {
      let queryParams = filters.map(filter => {
        return `${filter.columnName}=${filter.value}`
      });

      queryString = '/contacts/search/?';

      for (let i = 0; i < queryParams.length; i++) {
        let qParam = queryParams[i];
        queryString = queryString.concat(qParam);
        if (i !== queryParams.length - 1){
          queryString = queryString.concat('&');
        }
      }
    }

    return queryString;
  }

  detailHandler(editingRowIds) {
    // let rowPos = editingRowIds[0];
    // let { actions, navigator } = this.props;
    // let { rows } = this.state;
    // let id = rows[rowPos].id;

    // actions.routing(id);

  }

  loadData(search) {
    let queryString = this.queryString(search);

    this.setState({
      loading: true,
      rows: [],
    });
    Api.get(queryString)
      .then(data => {
        this.setState({
          rows: data,
          loading: false,
        });
      })
      .catch(() => this.setState({ loading: false }));
  }
  
  render() {
    const {
      rows,
      columns,
      tableColumnExtensions,
      sorting,
      pageSize,
      pageSizes,
      currentPage,
      totalCount,
      loading,
      showFilters,
    } = this.state;

    return (
      <Paper style={{ position: 'relative' }}>
        <Grid
          rows={rows}
          columns={columns}
        >
          <FilteringState onFiltersChange={this.changeFilters}/>
          <SortingState
            sorting={sorting}
            onSortingChange={this.changeSorting}
          />
          <PagingState
            currentPage={currentPage}
            onCurrentPageChange={this.changeCurrentPage}
            pageSize={pageSize}
            onPageSizeChange={this.changePageSize}
          />
          <IntegratedSorting />
          <IntegratedPaging />
          <CustomPaging
            totalCount={totalCount}
          />
          <EditingState
            onCommitChanges={(this.commitChanges)}
            onEditingRowIdsChange={this.changeEditingRowIds} 
          />
          <Table
            columnExtensions={tableColumnExtensions}
          />
           {showFilters && <TableFilterRow />}
          <TableEditColumn
            width={120}
            showEditCommand
            commandComponent={Command}
          />
          <TableColumnVisibility
            defaultHiddenColumnNames={['customer']}
          />
          <Toolbar />
          <FilterToggle toggleFilter={() => this.setState({ showFilters: !this.state.showFilters })}/>
          <RefreshToggle updateTable={this.loadData.bind(this, 'refresh')}/>
          <TableHeaderRow showSortingControls />
          <PagingPanel
            pageSizes={pageSizes}
          />
          
        </Grid>
        {loading && <Loading />}
      </Paper>
    );
  }
}

export default ContactList;