import { applyMiddleware, createStore } from 'redux';
import rootReducer from '../reducers';

import logger from 'redux-logger';

export default function configureStore(initialState) {
  const store = createStore(
    rootReducer,
    initialState,
    process.env && process.env.NODE_ENV && process.env.NODE_ENV === 'development' ? applyMiddleware(logger): undefined,
  );

  return store;
}
