const config = {
  DATE_FORMAT: "YYYY-MM-DD",
};

export default config;