import { combineReducers } from 'redux';
import navigator from './navigator.reducer';

const rootReducer = combineReducers({
    navigator,
});

export default rootReducer;