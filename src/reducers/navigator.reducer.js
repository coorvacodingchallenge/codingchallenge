import { VER_DETALLE } from '../constants/ActionTypes';

const initialState = {
  currentEntityId: ''
};

export default function navigator(state = initialState, action) {
  switch (action.type) {
    case VER_DETALLE:
      return {
        ...state,
        currentEntityId: action.entityId
      }

    default:
      return state;
  }
}
